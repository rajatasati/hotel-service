package com.javaworld.hotel.api.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class HotelService {

	public static final String WELCOME_TO_HOTEL_SERVICE = "Welcome to Hotel Services";

	public String getWelcomeMessage() {
		return WELCOME_TO_HOTEL_SERVICE;
	}

	public String getHotelById(String id) {
		return "Hotel ID : " + id;
	}

	public List<String> getHotelsList() {
		return Arrays.asList("Hotel1", "Hotel2", "Hotel3");
	}

}
