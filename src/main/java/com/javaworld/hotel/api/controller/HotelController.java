package com.javaworld.hotel.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.javaworld.hotel.api.service.HotelService;

@RestController
public class HotelController {

	@Autowired
	private HotelService hotelService;

	@GetMapping(path = "/welcome")
	public ResponseEntity<String> welcome() {
		return ResponseEntity.ok(hotelService.getWelcomeMessage());
	}

	@GetMapping(path = "/hotels/{id}")
	public ResponseEntity<String> getHotelById(@PathVariable("id") String id) {
		return ResponseEntity.ok(hotelService.getHotelById(id));
	}

	@GetMapping(path = "/hotels")
	public ResponseEntity<List<String>> getHotelsList() {
		return ResponseEntity.ok(hotelService.getHotelsList());
	}

}
